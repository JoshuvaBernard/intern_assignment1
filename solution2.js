/*
Alex and Bob are playing a game. Rules of the game are quite simple. Initially there are N boxes containing
chocolates on the table. In each turn, a player can choose one box and remove it from the table. Each player
wants to maximise the total number of chocolates taken by him. Alice takes the first turn.
Please help Alice find the maximum number of chocolates she can have assuming that both players play
optimally.
The first line of each test case contains a single integer N denoting the number of boxes. The second line contains
N space separated integers C1, C2, ..., CN denoting the number of chocolates in each box.
For each test case, output a single line containing the maximum number of chocolates that Alice can have.


Sample Input:
3
1 2 3
Output
4
*/

// Install promt-sync using > npm install prompt-sync
const prompt = require("prompt-sync")({ sigint: true });

const n =  prompt("");
let chocolates = prompt("");
let ChocolatesArr = chocolates.split(" ");
let numberOfChocolatesArr = [];
for (let i = 0; i < n; i++) {
    numberOfChocolatesArr.push(Number(ChocolatesArr[i]));
  }
if (n < 0) {
  console.log("Enter a valid number");
}
for (let i = 0; i < n; i++) {
  if (numberOfChocolatesArr[i] == 0) {
    console.log("Enter a valid numbers of chocolates");
  }
}

let alexArr = [];
let alexTotal = 0;

let bobArr = [];
let bobTotal = 0;

// returns the maximum value in the array
const maximum = function (n, numberOfChocolatesArr) {
    let maximum = 0;
    for (let i = 0; i < n; i++) {
      if (numberOfChocolatesArr[i] >= maximum) {
        maximum = numberOfChocolatesArr[i];
      }
    }
    return maximum;
};

// pushes the maximum number of chocolates available to the person array
const chocolatesTaken = function(arr){
    let currMax = maximum(n, numberOfChocolatesArr)
        arr.push(currMax);
        let index = numberOfChocolatesArr.indexOf(currMax)
        numberOfChocolatesArr.splice(index, 1)
}

for(let i = 1; i<=n; i++){
    if(i % 2 != 0){
        chocolatesTaken(alexArr)
    } else {
        chocolatesTaken(bobArr)
    }
}

// prints the total number of chocolates the player have
const totalChocolates = function(arr){
    let sum = 0;
    for(let i = 0; i < arr.length; i++){
        sum += arr[i];
    }
    return sum;
}

console.log(`Alex has ${totalChocolates(alexArr)}`);
console.log(`Bob has ${totalChocolates(bobArr)}`);






// for(let i = 1; i<=n; i++){
//     if(i % 2 != 0){
//         let currMax = maximum(n, numberOfChocolatesArr)
//         alexArr.push(currMax);
//         let index = numberOfChocolatesArr.indexOf(currMax)
//         numberOfChocolatesArr.splice(index, 1)
//         console.log(numberOfChocolatesArr);
//     } else {
//         let currMax = maximum(n, numberOfChocolatesArr)
//         bobArr.push(currMax);
//         let index = numberOfChocolatesArr.indexOf(currMax)
//         numberOfChocolatesArr.splice(numberOfChocolatesArr.indexOf(currMax), 1)
//         console.log(numberOfChocolatesArr);
//     }
// }