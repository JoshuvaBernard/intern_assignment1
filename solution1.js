
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});
rl.question("", function (n) {
  rl.question("", function (chocolates) {
    // split and sort
    let ChocolatesArr = chocolates.split(" ");
    let numberOfChocolatesArr = [];
    let alexChocolates = 0;
    let bobChocolates = 0;
    for (let i = 0; i < n; i++) {
      numberOfChocolatesArr.push(Number(ChocolatesArr[i]));
    }
    numberOfChocolatesArr.sort();
    //checks for wrong input
    if (n < 0) {
      console.log("Enter a valid number");
      
    }
    for (let i = 0; i < n; i++) {
      if (numberOfChocolatesArr[i] == 0) {
        console.log("Enter a valid numbers of chocolates");
      }
    }
    //choosing
    for (i = 1; i <= n; i++) {
      if (i % 2 != 0) {
        //alex's turn
        alexChocolates += numberOfChocolatesArr.pop();
      } else {
        // bob's turn
        bobChocolates += numberOfChocolatesArr.pop();
      }
    }
    console.log(`Alex has ${alexChocolates} Chocolates`);
    rl.close();
  });
});